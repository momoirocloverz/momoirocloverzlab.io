import Experience from '../Experience'
import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import EventEmitter from './EventEmitter'
export default class Resources extends EventEmitter {
  constructor(sources) {
    super()
    // this.experience = new Experience()
    this.sources = sources
    this.items = {}
    this.toLoad = this.sources.length
    this.loaded = 0
    this.setLoaders()
    this.startLoading()
  }
  setLoaders() {
    this.loaders = {}
    this.loaders.gltfLoader = new GLTFLoader()
    this.loaders.textureLoader = new THREE.TextureLoader()
    this.loaders.cubeTextureLoader = new THREE.CubeTextureLoader()
  }
  startLoading() {
    this.sources.forEach((ele) => {
      if (ele.type == 'cubeTexture') {
        this.loaders.cubeTextureLoader.load(ele.path, (file) => {
          this.sourceLoaded(ele, file)
        })
      }
      if (ele.type == 'texture') {
        this.loaders.textureLoader.load(ele.path, (file) => {
          this.sourceLoaded(ele, file)
        })
      }
      if (ele.type == 'gltfModel') {
        this.loaders.gltfLoader.load(ele.path, (file) => {
          this.sourceLoaded(ele, file)
        })
      }
    })
  }
  sourceLoaded(source, file) {
    this.items[source.name] = file
    this.loaded++
    if (this.loaded == this.toLoad) {
      this.trigger('ready')
    }
  }
}
