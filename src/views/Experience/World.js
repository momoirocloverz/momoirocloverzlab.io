import Environment from './utils/Environment'
import Floor from './utils/Floor'
import Fox from './utils/Fox'
import Experience from './Experience'
export default class World {
  constructor() {
    this.experience = new Experience()
    this.scene = this.experience.scene
    this.resources = this.experience.resources
    this.resources.on('ready', () => {
      console.log('ready')
      this.floor = new Floor()
      this.fox = new Fox()
      this.environment = new Environment()
    })
  }
  update() {
    if (this.fox) {
      this.fox.update()
    }
  }
}
