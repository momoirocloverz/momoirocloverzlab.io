import Experience from '../Experience'
import * as THREE from 'three'
import * as dat from 'lil-gui'
export default class DEbug {
  constructor(sources) {
    this.active = window.location.hash.indexOf('#debug') > -1
    if (this.active) {
      this.ui = new dat.GUI()
    }
  }
}
