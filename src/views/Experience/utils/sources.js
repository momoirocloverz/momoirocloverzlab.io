const HdrLink1 = new URL('../../../assets/HDR/px.jpg', import.meta.url).href
const HdrLink2 = new URL('../../../assets/HDR/nx.jpg', import.meta.url).href
const HdrLink3 = new URL('../../../assets/HDR/py.jpg', import.meta.url).href
const HdrLink4 = new URL('../../../assets/HDR/ny.jpg', import.meta.url).href
const HdrLink5 = new URL('../../../assets/HDR/pz.jpg', import.meta.url).href
const HdrLink6 = new URL('../../../assets/HDR/nz.jpg', import.meta.url).href
const GrassColor = new URL(
  '../../../assets/bigger/Fox/dirt/color.jpg',
  import.meta.url
).href
const GrassNormal = new URL(
  '../../../assets/bigger/Fox/dirt/normal.jpg',
  import.meta.url
).href
const FoxModel = new URL(
  '../../../assets/bigger/Fox/glTF-Binary/Fox.glb',
  import.meta.url
).href
export default [
  {
    name: 'environmentMapTexture',
    type: 'cubeTexture',
    path: [HdrLink1, HdrLink2, HdrLink3, HdrLink4, HdrLink5, HdrLink6],
  },
  {
    name: 'grassColorTexture',
    type: 'texture',
    path: GrassColor,
  },
  {
    name: 'grassNormalTexture',
    type: 'texture',
    path: GrassNormal,
  },
  {
    name: 'foxModel',
    type: 'gltfModel',
    path: FoxModel,
  },
]
